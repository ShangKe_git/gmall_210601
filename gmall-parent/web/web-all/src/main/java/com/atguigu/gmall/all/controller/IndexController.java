package com.atguigu.gmall.all.controller;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.model.product.BaseCategoryView;
import com.atguigu.gmall.product.client.ProductFeignClient;
import com.atguigu.gmall.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
* 商品详情页面的访问速度优化:分布式缓存,异步编排,静态化页面技术
* 分布式缓存:缓存雪崩,缓存穿透,缓存击穿   ;   AOP切面(Aspect)实现缓存:①.缓存的注解,②.切面(common-->service-util-->cache)
* 异步编排:使用线程池,串行化,并行化
* */

@Controller
public class IndexController {
    //https://www.gmall.com/

    @Autowired
    private ProductFeignClient productFeignClient;

    //首页
    @GetMapping("/")
    public String index(Model model) {
        List<Map> list = getData();
        System.out.println(JSONObject.toJSONString(list));
        //回显数据
        model.addAttribute("list", list);
        return "index/index";
    }

    //抽取数据
    private List<Map> getData() {
        /**
         * 办法1： POJO : 自定义POJO  同学课下可以完成一波
         * 办法2： 无敌版对象 Map
         * 办法3： JSONObject  底层依然是map
         java代码的速度 远远大于 DB的速度    尽量多写Java代码  少写SQL语句
         */
        List<Map>  list = new ArrayList<>();
        //查询数据库,获取分类视图的所有数据
       List<BaseCategoryView>  baseCategoryViewList = productFeignClient.getBaseCategoryViewList();
       //2:stream 流
        //Map1  K1:Long 一级分类ID   V1：List<BaseCategoryView> 长度 60  图书、音像、电子书刊
        //Map2  K2:Long 一级分类ID   V2：List<BaseCategoryView> 长度 25  手机
        Map<Long, List<BaseCategoryView>> listMapByCategory1Id = baseCategoryViewList.stream().collect(Collectors.groupingBy(
                BaseCategoryView::getCategory1Id));
        int index= 1;//行号
        //entryByCategory1Id:每个一级分类(K:V);entrySet是 java中 键-值 对的集合
        for(Map.Entry<Long,List<BaseCategoryView>> entryByCategory1Id : listMapByCategory1Id.entrySet()){
            //entryByCategory1Id.getValue():每个一级分级的Value
            List<BaseCategoryView> valueByCategory1Id = entryByCategory1Id.getValue();
            //一级分类
            HashMap mapByCategory1Id = new HashMap();
            mapByCategory1Id.put("index",index++);
            mapByCategory1Id.put("categoryId",entryByCategory1Id.getKey());
            mapByCategory1Id.put("categoryName",valueByCategory1Id.get(0).getCategory1Name());
            //==========================================================
            //二级分类的集合,二级分类和一级分类一样也是多种多个不同还包含三级分类
            Map<Long, List<BaseCategoryView>> listMapByCategory2Id = valueByCategory1Id.stream().collect(
                   Collectors.groupingBy(BaseCategoryView::getCategory2Id));
            List<Map> listByCategory2Id = new ArrayList<>();
            for(Map.Entry<Long,List<BaseCategoryView>> entryByCategory2Id :
                                listMapByCategory2Id.entrySet()){
                List<BaseCategoryView> valueByCategory2Id = entryByCategory2Id.getValue();
                HashMap mapByCategory2Id = new HashMap();
                mapByCategory2Id.put("categoryId",entryByCategory2Id.getKey());
                mapByCategory2Id.put("categoryName",valueByCategory2Id.get(0).getCategory1Name());
                //三级分类就是Map集合
                List<Map> listByCategory3Id =  valueByCategory2Id.stream().map((baseCategoryView)->{
                    Map map = new HashMap();
                    map.put("categoryId",baseCategoryView.getCategory3Id());
                    map.put("categoryName",baseCategoryView.getCategory3Name());
                    return map;
                }).collect(Collectors.toList());
                //二级分类 子集合
                mapByCategory2Id.put("categoryChild",listByCategory3Id);
                listByCategory2Id.add(mapByCategory2Id);
            }
            //一级分类 子集合
            mapByCategory1Id.put("categoryChild",listByCategory2Id);

            list.add(mapByCategory1Id);
        }
        return list;
    }


    //人为生成静态化页面  （静态化技术）
    // Thymeleaf
    //   1、页面标签   th:text..
    //   2、静态化技术   templateEngine   模板引擎

    @Autowired
    private TemplateEngine templateEngine;

    @GetMapping("/createHtml")
    @ResponseBody //将result对象转化未JSON串,显示在页面上
    public Result createHtml(){

        //输出流
        Writer out = null;

        try {
            // 模板（页面） + 数据  == 输出（静态化页面IO流写入到指定位置 Nginx映射位置）
            List<Map> list = getData();//这是上方的抽取数据:ctrl alt + m
            //数据
            Context context = new Context();
            context.setVariable("list",list);
            //写到文件夹指定位置
            out = new PrintWriter("E:\\temp\\index.html","UTF-8");
            //process : 处理 模板
            //参数1：模板路径
            //参数2：数据
            //参数3：输出流
            templateEngine.process("index/index",context,out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            try {
                if(out != null)
                    out.close();//关闭流
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Result.ok();
    }

}
