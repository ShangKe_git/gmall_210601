package com.atguigu.gmall.all.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 李旭
 * @date 2021/11/26 15:31
 * @Description:
 */
@Controller
public class CartInfoController {


    //加入购物车
    @GetMapping ("/addCart.html")
    public String addCart(Long skuId, Integer skuNum, Model model){
        //将商品加入到购物车
        System.out.println("将商品加入到购物车");
        //加入购物车成功
        return "cart/addCart";
    }
}
