package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.item.client.ItemFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

@Controller
public class ItemConteoller {

    @Autowired
    private ItemFeignClient itemFeignClient;
    //进入商品详情页面
    @GetMapping("/{skuId}.html")
    public String toItemPage(@PathVariable Long skuId, Map map){
        Map result = itemFeignClient.getItem(skuId);
        //将上面的resultMap直接整个全部放到新的Map中
        map.putAll(result);//K V
        //页面渲染
        return "item/index";
    }
}
