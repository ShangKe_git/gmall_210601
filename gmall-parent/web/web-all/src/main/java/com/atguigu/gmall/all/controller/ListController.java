package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.list.client.ListFeignClient;
import com.atguigu.gmall.model.list.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class ListController {

    @Resource
    private ListFeignClient listFeignClient;
    //开始搜索
    @GetMapping("/list.html")
    public String list(SearchParam searchParam, Model model){

        //远程调用搜索
        SearchResponseVo vo = listFeignClient.search(searchParam);

        //页面需要的数据
        List<Goods> goodsList = vo.getGoodsList();//商品集合
        List<SearchResponseAttrVo> attrsList = vo.getAttrsList();//平台属性集合
        List<SearchResponseTmVo> trademarkList = vo.getTrademarkList();//品牌集合
        Long totalPages = vo.getTotalPages();//总页数

        //页面渲染
        //1.回显关键字
        model.addAttribute("searchParam",searchParam);
        //2.品牌集合
        model.addAttribute("trademarkList",trademarkList);
        //3.平台属性集合
        model.addAttribute("attrsList",attrsList);
        //4.商品集合
        model.addAttribute("goodsList",goodsList);
        //5.分页
        model.addAttribute("totalPages",totalPages);
        model.addAttribute("pageNo",vo.getPageNo());
        //6.urlParam:拼接字符串
        String urlParam = buildUrlParam(searchParam);
        model.addAttribute("urlParam",urlParam);
        //7.排序 综合 （升|降）  底色（红底|白底）
        Map orderMap = buildOrderMap(searchParam);
        model.addAttribute("orderMap",orderMap);
        //8回显品牌 //trademark=2:华为   0:2 1:华为 下标
        String trademarkParam = buildTrademarkParam(searchParam);
        model.addAttribute("trademarkParam",trademarkParam);
        //8.2:回显平台属性及值
        List<Map> propsParamList = buildPropsParamList(searchParam);
        model.addAttribute("propsParamList",propsParamList);

        return "list/index";


    }
    //8.2:回显平台属性及值
    private List<Map> buildPropsParamList(SearchParam searchParam) {
        //  //props=23:4G:运行内存    ID:平台属性值:平台属性名称
        String[] props = searchParam.getProps();
        if(null != props && props.length > 0){
            return Arrays.stream(props).map(prop -> {
                Map map = new HashMap();
                String[] p = prop.split(":");
                map.put("attrId",p[0]);
                map.put("attrName",p[2]);
                map.put("attrValue",p[1]);
                return map;
            }).collect(Collectors.toList());
        }
        return null;
    }

    //回显品牌
    private String buildTrademarkParam(SearchParam searchParam) {
        String trademark = searchParam.getTrademark();
        if (!StringUtils.isEmpty(trademark)) {
            String[] t = trademark.split(":");
            return "品牌:" + t[1];
        }
        return null;
    }

    //排序 综合 （升|降）  底色（红底|白底）
    private Map buildOrderMap(SearchParam searchParam) {
        Map orderMap = new HashMap();
        // order=1:asc 1:desc  2:asc 2:desc
        String order = searchParam.getOrder();
        if(!StringUtils.isEmpty(order)){
            String[] o = order.split(":");
            orderMap.put("type",o[0]);
            orderMap.put("sort",o[1]);
        }else{
            orderMap.put("type","1");
            orderMap.put("sort","desc");
        }
        return orderMap;

    }

    //当前基础路径
    private String buildUrlParam(SearchParam searchParam) {
        StringBuilder sb = new StringBuilder();

        sb.append("http://list.gmall.com/list.html?");
        //关键字
        String keyword = searchParam.getKeyword();
        if(!StringUtils.isEmpty(keyword)){
            sb.append("keyword=").append(keyword);
        }
        //过滤条件
        //品牌
        String trademark = searchParam.getTrademark();
        if(!StringUtils.isEmpty(trademark)){
            sb.append("&trademark=").append(trademark);
        }
        //平台属性
        String[] props = searchParam.getProps();
        if(null != props && props.length > 0){
            for (String prop : props) {
                sb.append("&props=").append(prop);
            }
        }
        return sb.toString();
    }
}
