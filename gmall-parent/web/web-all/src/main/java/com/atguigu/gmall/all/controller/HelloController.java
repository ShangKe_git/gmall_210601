package com.atguigu.gmall.all.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class HelloController {

    //Thymeleafr入门

    @GetMapping("/hello")
    public String hello(Model model){
        model.addAttribute("world","和之国-->索隆");
        List<Map> listMap = new ArrayList<>();
        Map map1 = new HashMap();
        map1.put("addr","和之国");
        map1.put("name","路飞");
        Map map2 = new HashMap();
        map2.put("addr","恶魔岛");
        map2.put("name","索隆");
        Map map3 =new HashMap();
        map3.put("addr","人妖岛");
        map3.put("name","山治");
        listMap.add(map1);
        listMap.add(map2);
        listMap.add(map3);
        model.addAttribute("listMap",listMap);
        model.addAttribute("num",9);

        model.addAttribute("gname","<span style=\"color:green\">宝强</span>");
        return "hello";


    }
}
