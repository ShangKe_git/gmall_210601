package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.*;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface ManageService {
    //查看一级分类
    List<BaseCategory1> getCategory1();
    //查看二级分类
    List<BaseCategory2> getCategory2(Long category1Id);
    //查看三级分类
    List<BaseCategory3> getCategory3(Long category2Id);
    //根据分类的ID 查询平台属性及属性值集合
    List<BaseAttrInfo> attrInfoList(Long category1Id, Long category2Id, Long category3Id);
    //保存平台属性和属性值
    void saveAttrInfo(BaseAttrInfo baseAttrInfo);
    //查询品牌列表分页结果
    IPage<BaseTrademark> baseTrademarkList(Integer page, Integer limit);
    //品牌保存
    void baseTrademarkSave(BaseTrademark baseTrademark);
    //删除品牌
    void remove(Long id);
    //根据attrId 查询平台属性对象
    BaseAttrInfo getAttrInfo(Long attrId);
    //商品 分页查询
    IPage<SpuInfo> spuPage(Integer page, Integer limit, Long category3Id);
    //查看品牌列表
    List<BaseTrademark> getTrademarkList();
    //查看基本销售属性泪飙
    List<BaseSaleAttr> baseSaleAttrList();
    //商品保存
    void saveSpuInfo(SpuInfo spuInfo);
    //库存 SKu分页查询
    IPage skuPage(Integer page, Integer limit);
    //根据spuId 查询 商品的所有图片
    List<SpuImage> spuImageList(Long spuId);
    //根据spuId 查询销售属性及属性值
    List<SpuSaleAttr> spuSaleAttrList(Long spuId);
    //保存 SKUInfo
    void saveSkuInfo(SkuInfo skuInfo);
    //上架
    void onSale(Long skuId);
    //下架
    void cancelSale(Long skuId);
    //1:库存信息 及库存的所有图片  条件：库存Id
    SkuInfo getSkuInfo(Long skuId);
    //2、上面的库存信息中有三级分类的ID  所以才可以查询一二三级分类
    BaseCategoryView getBaseCategoryView(Long category3Id);
    //3:要求价格单独查询   从MySQL中查询
    BigDecimal getPrice(Long skuId);
    //4:查询销售属性及属性值并选中
    List<SpuSaleAttr> selectSpuSaleAttrListCheckBySku(Long spuId, Long skuId);
    //5组合数据查询{"1|5|7 ":"1" , "1|4|7":"2"},对应的sku_id
    Map getSaleAttrValuesBySpu(Long spuId);
    //6:查询分类视图中所有的分类数据
    List<BaseCategoryView> getBaseCategoryViewList();
    //8:skuInfo与平台属性及值的关联数据
    List<SkuAttrValue> getSkuAttrValueList(Long skuId);
    //7:根据品牌ID 查询品牌集合
    BaseTrademark getBaseTrademark(Long tmId);
}
