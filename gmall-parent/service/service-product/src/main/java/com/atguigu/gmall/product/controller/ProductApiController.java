package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.model.product.*;
import com.atguigu.gmall.product.service.ManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/product")
public class ProductApiController {

    @Autowired
    private ManageService manageService;

    //1:库存信息 及库存的所有图片  条件：库存Id
    // inner:表示自己这个微服务提供数据是内部数据  不可以暴露到外网上
    @GetMapping("/inner/getSkuInfo/{skuId}")
    public SkuInfo getSkuInfo(@PathVariable Long skuId) {
        return manageService.getSkuInfo(skuId);
    }


    //2、上面的库存信息中有三级分类的ID  所以才可以查询一二三级分类
    @GetMapping("/inner/getBaseCategoryView/{category3Id}")
    public BaseCategoryView getBaseCategoryView(@PathVariable Long category3Id) {

        return manageService.getBaseCategoryView(category3Id);
    }

    //3:要求价格单独查询   从MySQL中查询
    @GetMapping("/inner/getPrice/{skuId}")
    public BigDecimal getPrice(@PathVariable Long skuId) {
        return manageService.getPrice(skuId);
    }

    //4:查询销售属性及属性值并选中
    @GetMapping("/inner/selectSpuSaleAttrListCheckBySku/{spuId}/{skuId}")
    public List<SpuSaleAttr> selectSpuSaleAttrListCheckBySku(@PathVariable Long spuId
            , @PathVariable Long skuId) {

        return manageService.selectSpuSaleAttrListCheckBySku(spuId, skuId);
    }

    //5组合数据查询{"1|5|7 ":"1" , "1|4|7":"2"},对应的sku_id
    @GetMapping("/inner/getSaleAttrValuesBySpu/{spuId}")
    public Map getSaleAttrValuesBySpu(@PathVariable Long spuId) {
        return manageService.getSaleAttrValuesBySpu(spuId);
    }

    //6:查询分类视图中所有的分类数据
    //@GetMapping("/api/product/inner/getBaseCategoryViewList")
    @GetMapping("/inner/getBaseCategoryViewList")
    public List<BaseCategoryView> getBaseCategoryViewList() {
        return manageService.getBaseCategoryViewList();
    }
    //7:根据品牌ID 查询品牌集合
    @GetMapping("/getBaseTrademark/{tmId}")
    public BaseTrademark getBaseTrademark(@PathVariable Long tmId){
        return manageService.getBaseTrademark(tmId);
    }
    //8:skuInfo与平台属性及值的关联数据
    @GetMapping("/inner/getSkuAttrValueList/{skuId}")
    public List<SkuAttrValue> getSkuAttrValueList(@PathVariable Long skuId){
      return manageService.getSkuAttrValueList(skuId);
    }

}

