package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.product.utils.FastDFSUtils;
import com.atguigu.gmall.result.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 *  文件上传
 *
 *  文件下载
 *
 *  文件查询
 */
@RestController
@RequestMapping("/admin/product")
public class FileController {

    @Value("${image.url}")//读取配置文件中的值
    private String imageUrl;


    //文件上传
    @PostMapping("/fileUpload")
    public Result fileUpload(MultipartFile file) throws Exception{//全局异常处理器
        System.out.println(file.getOriginalFilename());
        System.out.println(file.getName());
        System.out.println(file.getSize());

        String path = FastDFSUtils.uploadFile(file.getBytes(), file.getOriginalFilename());
        System.out.println(imageUrl + path);
        return Result.ok(imageUrl + path);//整个URL返回到页面上
    }
    //文件下载

    //文件的删除


}
