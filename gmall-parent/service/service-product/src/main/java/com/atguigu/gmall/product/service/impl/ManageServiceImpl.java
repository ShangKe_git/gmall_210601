package com.atguigu.gmall.product.service.impl;

import com.atguigu.gmall.common.cache.GmallCache;
import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.model.product.*;
import com.atguigu.gmall.product.mapper.*;
import com.atguigu.gmall.product.service.ManageService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;


@Service
public class ManageServiceImpl implements ManageService {

    @Autowired
    private BaseCategory1Mapepr baseCategory1Mapepr;
    @Autowired
    private BaseCategory2Mapper baseCategory2Mapper;
    @Autowired
    private BaseCategory3Mapper baseCategory3Mapper;
    @Autowired
    private BaseAttrInfoMapper baseAttrInfoMapper;
    @Autowired
    private BaseAttrValueMapper baseAttrValueMapper;
    @Autowired
    private BaseTrademarkMapper baseTrademarkMapper;
    @Autowired
    private SpuInfoMapper spuInfoMapper;
    @Autowired
    private BaseSaleAttrMapper baseSaleAttrMapper;
    @Autowired
    private SpuImageMapper spuImageMapper;
    @Autowired
    private SpuSaleAttrMapper spuSaleAttrMapper;
    @Autowired
    private SpuSaleAttrValueMapper spuSaleAttrValueMapper;
    @Autowired
    private SkuInfoMapper skuInfoMapper;
    @Autowired
    private SkuImageMapper skuImageMapper;
    @Autowired
    private SkuAttrValueMapper skuAttrValueMapper;
    @Autowired
    private SkuSaleAttrValueMapper skuSaleAttrValueMapper;
    @Autowired
    private BaseCategoryViewMapper baseCategoryViewMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RedissonClient redissonClient;//从springIOC容器中获取

    //查看一级分类
    @Override
    public List<BaseCategory1> getCategory1() {

        return baseCategory1Mapepr.selectList(null);
    }

    //查看二级分类
    @Override
    public List<BaseCategory2> getCategory2(Long category1Id) {
        return baseCategory2Mapper.selectList(new QueryWrapper<BaseCategory2>()
                .eq("category1_id", category1Id));

    }

    //查看三级分类
    @Override
    public List<BaseCategory3> getCategory3(Long category2Id) {
        return baseCategory3Mapper.selectList(new QueryWrapper<BaseCategory3>()
                .eq("category2_id", category2Id));
    }

   /* //根据分类的ID 查询平台属性及属性值集合
    @Override
    public List<BaseAttrInfo> attrInfoList(Long category1Id, Long category2Id, Long category3Id) {

        return baseAttrInfoMapper.attrInfoList(category1Id,category2Id,category3Id);
    }*/

    //根据分类的ID 查询平台属性及属性值集合
    @Override
    public List<BaseAttrInfo> attrInfoList(Long category1Id, Long category2Id, Long category3Id) {
        return baseAttrInfoMapper.attrInfoList(category1Id, category2Id, category3Id);
    }

    //保存平台属性和属性值
    @Override
    @Transactional//打开事务
    public void saveAttrInfo(BaseAttrInfo baseAttrInfo) {
        //保存平台属性
        baseAttrInfoMapper.insert(baseAttrInfo);
        //保存平台属性值
        List<BaseAttrValue> attrValueList = baseAttrInfo.getAttrValueList();
        //判断平台属性值是否为空
        if (!CollectionUtils.isEmpty(attrValueList)) {
            attrValueList.forEach((attrValue) -> {
                //设置平台属性的外键
                attrValue.setAttrId(baseAttrInfo.getId());
                //
                baseAttrValueMapper.insert(attrValue);
            });
        }
    }

    //查询品牌列表分页结果
    @Override
    public IPage<BaseTrademark> baseTrademarkList(Integer page, Integer limit) {
        Page<BaseTrademark> baseTrademarkPage = new Page<>(page, limit);
        IPage<BaseTrademark> iPage = baseTrademarkMapper.selectPage(baseTrademarkPage, null);
        return iPage;
    }
    //更新品牌信息

    //品牌保存
    @Override
    public void baseTrademarkSave(BaseTrademark baseTrademark) {

        baseTrademarkMapper.insert(baseTrademark);
    }

    //删除品牌
    @Override
    public void remove(Long id) {
        baseTrademarkMapper.deleteById(id);
    }

    //根据id获取平台属性
    @Override
    public BaseAttrInfo getAttrInfo(Long attrId) {
        BaseAttrInfo baseAttrInfo = baseAttrInfoMapper.selectById(attrId);
        // 查询到最新的平台属性值集合数据放入平台属性中！
        baseAttrInfo.setAttrValueList(getAttrValueList(attrId));
        return baseAttrInfo;
    }

    //商品 分页查询
    @Override
    public IPage<SpuInfo> spuPage(Integer page, Integer limit, Long category3Id) {
        return spuInfoMapper.selectPage(new Page<SpuInfo>(page, limit),
                new QueryWrapper<SpuInfo>().eq("category3_id", category3Id));
    }

    //查看品牌列表
    @Override
    public List<BaseTrademark> getTrademarkList() {

        return baseTrademarkMapper.selectList(null);
    }

    //查看基本销售列表
    @Override
    public List<BaseSaleAttr> baseSaleAttrList() {
        return baseSaleAttrMapper.selectList(null);
    }

    //商品保存
    @Override
    public void saveSpuInfo(SpuInfo spuInfo) {
        //1.商品表
        spuInfoMapper.insert(spuInfo);
        //2.图片表
        List<SpuImage> spuImageList = spuInfo.getSpuImageList();
        if (!spuImageList.isEmpty() && spuImageList.size() != 0) {
            spuImageList.forEach(spuImage -> {
                //外键
                spuImage.setSpuId(spuInfo.getId());
                spuImageMapper.insert(spuImage);
            });
        }
        //3.商品与销售属性表的关联表
        List<SpuSaleAttr> spuSaleAttrList = spuInfo.getSpuSaleAttrList();
        if (!spuSaleAttrList.isEmpty() && spuSaleAttrList.size() != 0) {
            spuSaleAttrList.forEach(spuSaleAttr -> {
                spuSaleAttr.setSpuId(spuInfo.getId());
                spuSaleAttrMapper.insert(spuSaleAttr);
                //4.商品与销售属性值的关联表
                List<SpuSaleAttrValue> spuSaleAttrValueList = spuSaleAttr.getSpuSaleAttrValueList();
                if (!spuSaleAttrValueList.isEmpty() && spuSaleAttrValueList.size() != 0) {
                    spuSaleAttrValueList.forEach(spuSaleAttrValue -> {
                        //销售属性名称
                        spuSaleAttrValue.setSaleAttrName(spuSaleAttr.getSaleAttrName());
                        //外键
                        spuSaleAttrValue.setSpuId(spuInfo.getId());
                        spuSaleAttrValueMapper.insert(spuSaleAttrValue);
                    });
                }
            });
        }


    }

    //库存 SKu分页查询
    @Override
    public IPage skuPage(Integer page, Integer limit) {

        return skuInfoMapper.selectPage(new Page<>(page, limit), null);
    }

    //根据spuId 查询 商品的所有图片
    @Override
    public List<SpuImage> spuImageList(Long spuId) {

        return spuImageMapper.selectList(new QueryWrapper<SpuImage>()
                .eq("spu_id", spuId));
    }

    //根据spuId 查询销售属性及属性值
    @Override
    public List<SpuSaleAttr> spuSaleAttrList(Long spuId) {
        return spuSaleAttrMapper.spuSaleAttrList(spuId);
    }

    @Override
    public void saveSkuInfo(SkuInfo skuInfo) {

        //1、Sku_info   核心表
        skuInfoMapper.insert(skuInfo);
        //2、sku_image库存的图片表   某一个颜色的所有图片
        List<SkuImage> skuImageList = skuInfo.getSkuImageList();
        if (!skuImageList.isEmpty() && skuImageList.size() != 0) {
            skuImageList.forEach(skuImage -> {
                skuImage.setSkuId(skuInfo.getSpuId());
                skuImageMapper.insert(skuImage);
            });
        }
        //3、sku_attr_value 库存的平台属性及属性值表
        List<SkuAttrValue> skuAttrValueList = skuInfo.getSkuAttrValueList();
        if (!skuAttrValueList.isEmpty() && skuAttrValueList.size() != 0) {
            skuAttrValueList.forEach(skuAttrValue -> {
                skuAttrValue.setSkuId(skuInfo.getId());
                skuAttrValueMapper.insert(skuAttrValue);
            });
        }
        //sku_sale_attr_value 库存的销售属性及属性值表
        List<SkuSaleAttrValue> skuSaleAttrValueList = skuInfo.getSkuSaleAttrValueList();
        if (!skuSaleAttrValueList.isEmpty() && skuSaleAttrValueList.size() != 0) {
            skuSaleAttrValueList.forEach(skuSaleAttrValue -> {
                //skuId
                skuSaleAttrValue.setSkuId(skuInfo.getId());
                //spuId
                skuSaleAttrValue.setSpuId(skuInfo.getSpuId());
                skuSaleAttrValueMapper.insert(skuSaleAttrValue);
            });
        }

    }

    //上架
    @Override
    public void onSale(Long skuId) {
        SkuInfo skuInfo = new SkuInfo();
        skuInfo.setSpuId(skuId);
        skuInfo.setIsSale(SkuInfo.ISSALE);//解决硬编码问题
        skuInfoMapper.updateById(skuInfo);//update sku_info set is_sale =1 where id = ?
        //TODO 发消息通知搜索微服务 添加商品的索引

    }

    //下架
    @Override
    public void cancelSale(Long skuId) {
        SkuInfo skuInfo = new SkuInfo();
        skuInfo.setSpuId(skuId);
        skuInfo.setIsSale(SkuInfo.CANCELSALE);//解决硬编码问题
        skuInfoMapper.updateById(skuInfo);//update sku_info set is_sale =1 where id = ?
        //TODO 发消息通知搜索微服务 删除商品的索引
    }

//    //1:库存信息 及库存的所有图片  条件：库存Id
//    //使用RedissonClient来实现分布式锁,就是当本地锁的方式来使用就可以实现分布式锁
//    @Override
//    public SkuInfo getSkuInfo(Long skuId) {
//        //查看k时,最好拼接数据
//        String cacheKey = RedisConst.SKUKEY_PREFIX + skuId + RedisConst.SKUKEY_SUFFIX;
//        //1.查看缓存
//        SkuInfo skuInfo = (SkuInfo) redisTemplate.opsForValue().get(cacheKey);
//        //2.缓存中没有数据就直接返回
//        if (skuInfo == null) {
//            //3.缓存中没有,查DB
//            //skuInfo = skuInfoMapper.selectById(skuId);
//            //3.1加锁//缓存击穿:一个热点数据失效
//            //解决:加锁
//            String cacheLockKey = RedisConst.SKUKEY_PREFIX + skuId + RedisConst.SKULOCK_SUFFIX;
//            RLock lock = redissonClient.getLock(cacheLockKey);
//            //过期不候的锁
//            //参数1： 过期不候的过期是多长时间
//            //参数2： 获取到锁之后 锁的过期时间
//            //参数3： 单位
//            try {
//                boolean islock = lock.tryLock(1, 1, TimeUnit.SECONDS);
//                if (islock) {
//                    //获取到了锁
//                    //库存信息
//                    try {
//                        //3.缓存中没有,查DB
//                        skuInfo = skuInfoMapper.selectById(skuId);
//                        if (skuInfo != null) {
//                            List<SkuImage> skuImageList = skuImageMapper.selectList(new QueryWrapper<SkuImage>()
//                                    .eq("sku_id", skuId));
//                            skuInfo.setSkuImageList(skuImageList);
//                            //4.DB中存在则复制一份到缓存中
//                            //缓存雪崩:设置相同的过期时间,可能同时过期后到值DB流量大,造成雪崩,一连串数据失效
//                            //解决:过期时间后在加上随机时间
//                            redisTemplate.opsForValue().set(cacheKey, skuInfo, RedisConst.SKUKEY_TIMEOUT + new Random().nextInt(300), TimeUnit.SECONDS);
//
//                        } else {
//                            //代码执行到此处,说明DB中也没有数据
//                            //缓存穿透,查询一个不存在的数据,由于缓存无法命中就去查询数据库, 此时数据库中也没有数据,由于容错考虑,也没有将一个
//                            //null值存储到缓存中,这将导致一值去DB中查询,在流量大时，可能DB就挂掉了，要是有人利用不存在的key频繁攻击我们的应用，这就是漏洞。
//                            //解决：空结果也要进行缓存,不过缓存事件会很短,不超过5分钟;企业级防火墙屏蔽,nginx限流
//                            skuInfo = new SkuInfo();
//                            redisTemplate.opsForValue().set(cacheKey, skuInfo, 5, TimeUnit.MINUTES);
//
//                        }
//                    } finally {
//                        //解锁
//                        lock.unlock();
//                    }
//                } else {
//                    //未获取到锁,去缓存中查询数据即可,DB中数据复制到缓存中和直接去缓存中查有时间差
//                    try {
//                        TimeUnit.SECONDS.sleep(2);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    //去缓存中查询数据
//                    skuInfo = (SkuInfo) redisTemplate.opsForValue().get(cacheKey);
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//        }
//        //5.返回结果
//        return skuInfo;
//    }
@Override
public SkuInfo getSkuInfo(Long skuId) {
    //缓存Key
    String cacheKey = RedisConst.SKUKEY_PREFIX + skuId + RedisConst.SKUKEY_SUFFIX;  //本次是查询SkuInfo的数据 拼接了 skuId
    //1:查询缓存
    SkuInfo skuInfo = (SkuInfo) redisTemplate.opsForValue().get(cacheKey);
    //2:有 直接返回
    if(null == skuInfo){
        //3:没有 查询DB
        String cacheLockKey = RedisConst.SKUKEY_PREFIX + skuId + RedisConst.SKULOCK_SUFFIX;
        //3.1:加锁
        RLock lock = redissonClient.getLock(cacheLockKey);
        //过期不候的锁
        //参数1： 过期不候的过期是多长时间
        //参数2： 获取到锁之后 锁的过期时间
        //参数3： 单位
        try {
            boolean isLock = lock.tryLock(1, 1, TimeUnit.SECONDS);
            if(isLock){
                //获取到了锁
                //库存信息
                try {
                    skuInfo = skuInfoMapper.selectById(skuId);
                    if(null != skuInfo){
                        //库存的图片
                        List<SkuImage> skuImageList = skuImageMapper.selectList(new QueryWrapper<SkuImage>()
                                .eq("sku_id", skuId));
                        skuInfo.setSkuImageList(skuImageList);
                        //4:再保存一份到缓存中
                        redisTemplate.opsForValue().set(cacheKey,skuInfo,
                                RedisConst.SKUKEY_TIMEOUT + new Random().nextInt(300), //解决缓存雪崩
                                TimeUnit.SECONDS);//过期时间一天+随机数（5分钟之内）
                    }else{
                        //代码能执行在此处  说明DB中也没有数据
                        //缓存的穿透问题  不让访问把DB弄挂掉
                        //将空结果保存到缓存中
                        skuInfo = new SkuInfo();// 不是NULL 是空结果  结果是skuInfo对象 对象中没有数据 叫空结果
                        redisTemplate.opsForValue().set(cacheKey,skuInfo,5,TimeUnit.MINUTES);//过期时间是5分钟
                    }
                } finally {
                    //解锁
                    lock.unlock();
                }
            }else{
                //未获取到锁  去获取缓存数据即可
                try {
                    TimeUnit.SECONDS.sleep(1);//1S   时间差
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                skuInfo = (SkuInfo) redisTemplate.opsForValue().get(cacheKey);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //5：统一返回
    return skuInfo;
}

    //上面的库存信息中有三级分类的ID  所以才可以查询一二三级分类
    @Override
    @GmallCache(prefix = "basCategoryView") //自定义AOP注解
    public BaseCategoryView getBaseCategoryView(Long category3Id) {
        //1.从redis缓存中查询
        //1.1缓存key
        String cacheKey = RedisConst.SKUKEY_PREFIX + category3Id +RedisConst.SKUKEY_SUFFIX;
        BaseCategoryView baseCategoryView = (BaseCategoryView) redisTemplate.opsForValue().get(cacheKey);
        if(baseCategoryView == null){
        //2.缓存中没有,就从DB中查询
            //2.1加锁
            String cahceLockKey = RedisConst.SKUKEY_PREFIX+ category3Id +RedisConst.SKULOCK_SUFFIX;
        //UUID:判断自己的锁是否是唯一标识
            String uuid = UUID.randomUUID().toString();
            //查询DB之前要求加锁,防止热点数据 大量请求访问 只能通过一个
            Boolean isLock = redisTemplate.opsForValue().setIfAbsent(cahceLockKey, uuid, 1, TimeUnit.SECONDS);
            //判断是否取到锁
            if(isLock){
                try {
                    baseCategoryView = baseCategoryViewMapper.selectById(category3Id);
                    if (baseCategoryView != null){
                        //在保存一份到缓存中
                        //设置过期时间,解决缓存雪崩问题,企业级防火墙,nginx限流
                        redisTemplate.opsForValue().set(cahceLockKey,baseCategoryView,RedisConst.SECKILL__TIMEOUT
                                +new Random().nextInt(300),TimeUnit.SECONDS);
                    }else {
                        //DB中也没有数据,就新建一个数据储存,解决缓存穿透
                       baseCategoryView = new BaseCategoryView();
                       redisTemplate.opsForValue().set(cahceLockKey,baseCategoryView,5,TimeUnit.MINUTES);
                    }
                } finally {
                    Object u = redisTemplate.opsForValue().get(cahceLockKey);
                    if(uuid.equals(u)){
                        redisTemplate.delete(cahceLockKey);
                    }


                }

            }else {
                //未取得锁,去获取缓存数据即可
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                baseCategoryView = (BaseCategoryView) redisTemplate.opsForValue().get(cacheKey);
            }

            }

        return baseCategoryView;
    }

    //3:要求价格单独查询   从MySQL中查询
    @Override
    public BigDecimal getPrice(Long skuId) {
        // select * from sku_info where id = ?
        // select price from sku_info where id = ?
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", skuId);
        queryWrapper.select("price");
        SkuInfo skuInfo = skuInfoMapper.selectOne(queryWrapper);
        if (null != skuInfo) {
            return skuInfo.getPrice();
        }
        return null;
    }

    //4:查询销售属性及属性值并选中
    @Override
    @GmallCache(prefix = "selectSpuSaleAttrListCheckBySku")
    public List<SpuSaleAttr> selectSpuSaleAttrListCheckBySku(Long spuId, Long skuId) {
        return spuSaleAttrMapper.selectSpuSaleAttrListCheckBySku(spuId, skuId);

    }

    //5组合数据查询{"1|5|7 ":"1" , "1|4|7":"2"},对应的sku_id
    @Override
    @GmallCache(prefix = "getSaleAttrValuesBySpu") //使用AOP切面实现缓存
    public Map getSaleAttrValuesBySpu(Long spuId) {
        //mapper查询出的是一个ListMap结合:中用一个个map键值对
        List<Map> skuValueIdsMap = skuSaleAttrValueMapper.getSkuValueIdsMap(spuId);
        Map result = new HashMap();
        skuValueIdsMap.forEach(map -> {
            //k:value_Ids  v:sku_id
            result.put(map.get("value_ids"), map.get("sku_id"));
        });

        return result;
    }
    //6:查询分类视图中所有的分类数据
    @Override
    public List<BaseCategoryView> getBaseCategoryViewList() {
        return  baseCategoryViewMapper.selectList(null);
    }
    //8:skuInfo与平台属性及值的关联数据
    @Override
    public List<SkuAttrValue> getSkuAttrValueList(Long skuId) {

        return skuAttrValueMapper.getSkuAttrValueList(skuId);
    }
    //7:根据品牌ID 查询品牌集合
    @Override
    public BaseTrademark getBaseTrademark(Long tmId) {
        return baseTrademarkMapper.selectById(tmId);
    }

    private List<BaseAttrValue> getAttrValueList(Long attrId) {
        //select * from baseAttrValue where attrId = ?
        QueryWrapper queryWrapper = new QueryWrapper<BaseAttrValue>();
        queryWrapper.eq("attr_id", attrId);
        List<BaseAttrValue> baseAttrValueList = baseAttrValueMapper.selectList(queryWrapper);
        return baseAttrValueList;
    }


}
