package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.model.product.BaseCategory1;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 李旭
 * @date 2021/11/13 14:44
 * @Description:
 */
public interface BaseCategory1Mapper extends BaseMapper<BaseCategory1> {
}
