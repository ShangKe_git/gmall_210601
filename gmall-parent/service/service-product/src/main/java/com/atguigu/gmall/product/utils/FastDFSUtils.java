package com.atguigu.gmall.product.utils;

import org.apache.commons.io.FilenameUtils;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient1;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ClassUtils;

public class FastDFSUtils {

    //日志  打注解
    private static Logger logger = LoggerFactory.getLogger(FastDFSUtils.class);

    //成员变量 大家公用的
    private static TrackerClient trackerClient = null;//跟踪器的地址  连接时间  通迅时间 等
    //提取出公共资源
    static {//JVM 方法中
        //开始上传文件
        String path = ClassUtils.getDefaultClassLoader().getResource("fdfs_client.conf").getPath();
        try {
            ClientGlobal.init(path);
            //1:上传连接请求
            trackerClient = new TrackerClient();
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("文件初始化失败");
        }

    }

    //文件的上传
    public static String uploadFile(byte[] file,String fileName){
        try {
            //2:连接Tracker
            TrackerServer trackerServer = trackerClient.getConnection();
            //3:StorageClient1 升级版
            StorageClient1 storageClient1 = new StorageClient1(trackerServer,null);

            //获取扩展名
            String ext = FilenameUtils.getExtension(fileName);

            //4:上传图片   meta:元数据信息  图片的长度是多少  图片是背景  图片描述
            String path = storageClient1.upload_file1(file, ext, null);

            return path;
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("文件上传失败");
        }
        return null;
    }

    //文件的下载


    //文件的删除





}
