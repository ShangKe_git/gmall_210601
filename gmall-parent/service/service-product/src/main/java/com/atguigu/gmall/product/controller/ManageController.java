package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.model.product.*;
import com.atguigu.gmall.product.service.ManageService;
import com.atguigu.gmall.result.Result;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/product")
public class ManageController {

    @Autowired
    private ManageService manageService;

    //获取一级分类集合
    @GetMapping("/getCategory1")
    public Result getCategory1(){
        List<BaseCategory1> baseCategory1List = manageService.getCategory1();
        return Result.ok(baseCategory1List);
    }

    //获取二级分类集合
    @GetMapping("/getCategory2/{category1Id}")
    public Result getCategory2( @PathVariable Long category1Id){
        List<BaseCategory2> baseCategory2List = manageService.getCategory2(category1Id);
        return Result.ok(baseCategory2List);

    }
    //获取三级分类集合
    @GetMapping("/getCategory3/{category2Id}")
    public Result getCategory3(@PathVariable Long category2Id){
     List<BaseCategory3> baseCategory3List =   manageService.getCategory3(category2Id);
        return Result.ok(baseCategory3List);
    }
    //根据分类的ID 查询平台属性及属性值集合
    @GetMapping("/attrInfoList/{category1Id}/{category2Id}/{category3Id}")
    public Result attrInfoList(
            @PathVariable Long category1Id,
            @PathVariable Long category2Id,
            @PathVariable Long category3Id
    ){
      List<BaseAttrInfo>  baseAttrInfoList= manageService.attrInfoList(category1Id,category2Id,category3Id);
        return Result.ok(baseAttrInfoList);

    }

    //保存平台属性及属性值
    @PostMapping("/saveAttrInfo")
    public Result saveAttrInfo(@RequestBody BaseAttrInfo baseAttrInfo){
        manageService.saveAttrInfo(baseAttrInfo);//默认向上抛出异常
        return Result.ok();//200  成功
    }

    /**
     * 根据attrId 查询平台属性对象
     */
    @GetMapping("getAttrValueList/{attrId}")
    public Result<List<BaseAttrValue>> getAttrValueList(@PathVariable("attrId") Long attrId) {
        BaseAttrInfo baseAttrInfo = manageService.getAttrInfo(attrId);
        List<BaseAttrValue> baseAttrValueList = baseAttrInfo.getAttrValueList();
        return Result.ok(baseAttrValueList);
    }

    //查询品牌列表分页结果
    @GetMapping("/baseTrademark/{page}/{limit}")
    public Result baseTrademarkList(@PathVariable Integer page,@PathVariable Integer limit){
        IPage<BaseTrademark> iPage = manageService.baseTrademarkList(page,limit);
        return Result.ok(iPage);
    }
    //保存品牌
    @PostMapping("/baseTrademark/save")
    public Result baseTrademarkSave(@RequestBody BaseTrademark baseTrademark){
        manageService.baseTrademarkSave(baseTrademark);
        return Result.ok();//200  成功
    }
    //删除品牌
    @GetMapping("/baseTrademark/remove/{id}")
    public Result remove(@PathVariable Long id){
        manageService.remove(id);
        return Result.ok();
    }
    //商品 分页查询
    @GetMapping("/{page}/{limit}")
    public Result spuPage(@PathVariable Integer page,@PathVariable Integer limit,Long category3Id){
        IPage<SpuInfo> spuInfoIPage = manageService.spuPage(page,limit,category3Id);
        return Result.ok(spuInfoIPage);//200  成功
    }
    //查询品牌列表
    @GetMapping("/baseTrademark/getTrademarkList")
    public Result getTrademarkList(){
        List<BaseTrademark> baseTrademarkList = manageService.getTrademarkList();
        return Result.ok(baseTrademarkList);
    }
    //查询销售属性列表  /baseSaleAttrList
    @GetMapping("/baseSaleAttrList")
    public Result baseSaleAttrList(){
        List<BaseSaleAttr> baseSaleAttrList = manageService.baseSaleAttrList();

        return Result.ok(baseSaleAttrList);
    }
    //商品保存
    @PostMapping("/saveSpuInfo")
    public Result saveSpuInfo(@RequestBody SpuInfo spuInfo){
        manageService.saveSpuInfo(spuInfo);
        return Result.ok();
    }
    //库存 SKu分页查询
    @GetMapping("/list/{page}/{limit}")
    public Result skuPage(@PathVariable Integer page,@PathVariable Integer limit){
      IPage iPage = manageService.skuPage(page,limit);
        return Result.ok(iPage);
    }
    //根据spuId 查询 商品的所有图片
    @GetMapping("/spuImageList/{spuId}")
    public Result spuImageList(@PathVariable Long spuId){
        List<SpuImage> spuImageList= manageService.spuImageList(spuId);
        return Result.ok(spuImageList);
    }
    //根据spuId 查询销售属性及属性值
    @GetMapping("/spuSaleAttrList/{spuId}")
    public Result spuSaleAttrList(@PathVariable Long spuId){
        List<SpuSaleAttr> spuSaleAttrList = manageService.spuSaleAttrList(spuId);
        return Result.ok(spuSaleAttrList);
    }
    //保存 SKUInfo
    @PostMapping("/saveSkuInfo")
    public Result saveSkuInfo(@RequestBody SkuInfo skuInfo){

        manageService.saveSkuInfo(skuInfo);
        return Result.ok();
    }


    //上架
    @GetMapping("/onSale/{skuId}")
    public Result onSale(@PathVariable Long skuId){
        manageService.onSale(skuId);
        return Result.ok();
    }
    //下架
    @GetMapping("/cancelSale/{skuId}")
    public Result cancelSale(@PathVariable Long skuId){

        manageService.cancelSale(skuId);
        return Result.ok();
    }

}
