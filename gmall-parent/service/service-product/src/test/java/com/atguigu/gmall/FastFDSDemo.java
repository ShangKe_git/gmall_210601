package com.atguigu.gmall;

import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient1;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.springframework.util.ClassUtils;



public class FastFDSDemo {
    public static void main(String[] args) throws Exception {
        //读取配置文件
      String path =  ClassUtils.getDefaultClassLoader().getResource("fdfs_client.conf").getPath();
        //D:\IdeaProjects\gmall-210601\gmall-parent\service\service-product\src\main\resources\fdfs_client.conf
        // init 底层IO流 读取你的指定的文件  IO流不认识相对路径 只绝对路径
        ClientGlobal.init(path);
        //1.上传连接请求
        TrackerClient trackerClient = new TrackerClient();
        //2.连接跟踪器
        TrackerServer connection = trackerClient.getConnection();
        //3.连接储存节点
        StorageClient1 storageClient1 = new StorageClient1(connection,null);
        //上传图片
        String p = storageClient1.upload_file1("C:\\Users\\95849\\Desktop\\壁纸\\71e52c67f5094e44b92ccaed93db15c5.jpg","jpg",null);
        System.out.println("http://192.168.100.88:8080/"+p);


    }
}
