package com.atguigu.gmall.user.controller;

import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.model.user.UserInfo;
import com.atguigu.gmall.result.Result;
import com.atguigu.gmall.user.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api/user/passport")
public class UserInfoApiController {
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private RedisTemplate redisTemplate;

    @PostMapping("/login")
    public Result login(@RequestBody UserInfo userInfo){
        //登录
    userInfo  = userInfoService.login(userInfo);
    if(userInfo == null){
        return Result.fail().message("用户名或密码不正确");
    }else {
        //3.1正确 //回显   昵称|令牌   JWT:头 载荷 加密  字符串
        String token = UUID.randomUUID().toString();
        redisTemplate.opsForValue().set(RedisConst.USER_LOGIN_KEY_PREFIX+token,userInfo.getId().toString(),RedisConst.USERKEY_TIMEOUT
        , TimeUnit.SECONDS);
        //3.2返回
        Map map = new HashMap<>();
        map.put("token",token);
        map.put("nickName",userInfo.getNickName());
        return Result.ok(map);
    }
    }

}
