package com.atguigu.gmall.user.service;

import com.atguigu.gmall.model.user.UserInfo;

public interface UserInfoService {
    //用户登录
    UserInfo login(UserInfo userInfo);
}
