package com.atguigu.gmall.item.controller;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.list.client.ListFeignClient;
import com.atguigu.gmall.model.product.BaseCategoryView;
import com.atguigu.gmall.model.product.SkuInfo;
import com.atguigu.gmall.model.product.SpuSaleAttr;
import com.atguigu.gmall.product.client.ProductFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;

/**
 *    商品详情  数据汇总及处理
 *    商品详情微服务
 */
@RestController
@RequestMapping("/api/item")
public class ItemApiController {

    @Autowired
    private ProductFeignClient productFeignClient;
    //注入线程池
    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;
    //商品热点评分
    @Autowired
    private ListFeignClient listFeignClient;

    /*
    *异步编排
    *completableFuture:开启线程:串行化,并行化
    * supplyAsync():callable->默认线程池,自定义线程池ThreaPollExecutor,有返回值
    * runAsync():runnable->默认线程池,自定义线程池ThreaPollExecutor,无返回值
    * 上一个任务(异常,返回值)-->当前任务(接受上一个任务的返回值,自己有无返回值,异常)-->下一个任务(接受当前任务的返回值)
    * 计算完成时回调方法:当CompletableFuture的计算结果完成，或者抛出异常的时候，可以执行特定的Action
    *线程串行化与并行化方法
    * 串行化:thenApply:当一个线程依赖另一个线程时，获取上一个任务返回的结果，并返回当前任务的返回值。
    * thenAccept方法：消费处理结果。接收任务的处理结果，并消费处理，无返回结果。
    * thenRun方法：只要上面的任务执行完成，就开始执行thenRun，只是处理完任务后，执行 thenRun的后续操作
    *
    * 多任务组合
    * allof().join():等待所有任务完成
    * anyof().join():只要一个任务完成
    * jion():阻塞,只有当任务完成时,才继续往下执行
    * */

    //数据汇总  处理
    @GetMapping("/getItem/{skuId}")
    public Map getItem(@PathVariable Long skuId){
        Map result = new HashMap<>();
        CompletableFuture<SkuInfo> skuInfoCompletableFuture = CompletableFuture.supplyAsync(() -> {
            //1:库存信息
            SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
            result.put("skuInfo", skuInfo);
            return skuInfo;//上一次任务的返回值
        }, threadPoolExecutor);

        //thenAccpet:接受上一次任务的返回值,但自己没有返回值
        CompletableFuture<Void> categoryViewCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync(skuInfo -> {
            //2:一二三级分类
            BaseCategoryView baseCategoryView = productFeignClient.getBaseCategoryView(skuInfo.getCategory3Id());
            result.put("categoryView", baseCategoryView);
        },threadPoolExecutor);// 接收上一个任务的返回值  但是自己无返回


        //和库存详情线程并行的价格新城,没有返回值
        CompletableFuture<Void> priceCompletableFuture = CompletableFuture.runAsync(() -> {
            //3:实时价格
            BigDecimal price = productFeignClient.getPrice(skuId);
            result.put("price", price);
        }, threadPoolExecutor);

        CompletableFuture<Void> spuSaleAttrListCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync(skuInfo -> {
            //4:销售属性及值及选中
            List<SpuSaleAttr> spuSaleAttrList = productFeignClient.
                    selectSpuSaleAttrListCheckBySku(skuInfo.getSpuId(), skuInfo.getId());
            result.put("spuSaleAttrList", spuSaleAttrList);
        },threadPoolExecutor);// 接收上一个任务的返回值  但是自己无返回

        CompletableFuture<Void> valuesSkuJsonCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync(skuInfo -> {
            //5:组合数据 对应 SKUID   页面上要的是JSON串
            Map saleAttrValuesBySpu = productFeignClient.getSaleAttrValuesBySpu(skuInfo.getSpuId());
            result.put("valuesSkuJson", JSONObject.toJSONString(saleAttrValuesBySpu));
        },threadPoolExecutor);// 接收上一个任务的返回值  但是自己无返回

        //商品热点评分
         CompletableFuture.runAsync(() -> {
            listFeignClient.hotScore(skuId, 1);
        }, threadPoolExecutor);

        //多任务组合
        CompletableFuture.allOf(skuInfoCompletableFuture,categoryViewCompletableFuture,
                priceCompletableFuture,spuSaleAttrListCompletableFuture,valuesSkuJsonCompletableFuture).join();
        //返回汇总结果
        return result;
    }
}
