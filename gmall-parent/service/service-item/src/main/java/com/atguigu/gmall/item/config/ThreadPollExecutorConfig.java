package com.atguigu.gmall.item.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//线程池,配置类
@Configuration
public class ThreadPollExecutorConfig {
    @Bean
    public ThreadPoolExecutor threadPoolExecutor() {
        return new ThreadPoolExecutor(
                5,//核心线程数
                10,//最大线程数
                3,//线程存活时间
                TimeUnit.SECONDS,//时间单位
                new ArrayBlockingQueue<>(10)//阻塞队列
        );
    }
}
