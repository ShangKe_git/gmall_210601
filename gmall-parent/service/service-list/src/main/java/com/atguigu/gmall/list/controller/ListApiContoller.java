package com.atguigu.gmall.list.controller;

import com.atguigu.gmall.list.service.ListService;
import com.atguigu.gmall.model.list.SearchParam;
import com.atguigu.gmall.model.list.SearchResponseVo;
import com.atguigu.gmall.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/*
*1.使用swagger 创建索引里
* 2.使用swagger 创建mappings映射
* 3.使用swagger 上架 删除商品
* */


@RestController
@RequestMapping("/api/list")
public class ListApiContoller {
    @Autowired
    private ListService listService;

    //创建索引库并mappings映射
    @GetMapping("inner/createIndex")
    public Result createIndex() {
        //创建索引库
        //创建mappings映射
        listService.createIndex();
        return Result.ok();
    }

    //上架:将商品添加到索引库
    @GetMapping("onSale/{skuId}")
    public Result onSale(@PathVariable Long skuId){
        listService.onSale(skuId);
        return Result.ok();
    }
    //下架:将商品从索引库中删除
    @GetMapping("cancelSale/{skuId}")
    public Result cancelSale(@PathVariable Long skuId){
        listService.cancelSale(skuId);
        return Result.ok();
    }

    //商品热度评分  更新操作
    @GetMapping("hotScore/{skuId}/{score}")
    public Result hotScore(@PathVariable Long skuId,@PathVariable Integer score){
        listService.hotScore(skuId,score);
        return Result.ok();
    }

    //开始搜索
    //SearchParam 封装了搜索数据 :关键字,品牌,平台属性 平台属性值,排序,分业
    //SearchResponseVo:返回页面需要数据的封装 : 品牌集合,分页列表(总记录数),排序集合,所有商品的顶头显示的筛选属性
    @PostMapping("/search") //SearchParam 封装了搜索数据 ; SearchResponseVo:返回页面需要数据的封装
    public SearchResponseVo search(@RequestBody SearchParam searchParam){
        return listService.search(searchParam);

    }
}
