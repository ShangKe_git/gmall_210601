package com.atguigu.gmall.list.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.list.dao.GoodsDao;
import com.atguigu.gmall.list.service.ListService;
import com.atguigu.gmall.model.list.*;
import com.atguigu.gmall.model.product.BaseCategoryView;
import com.atguigu.gmall.model.product.BaseTrademark;
import com.atguigu.gmall.model.product.SkuAttrValue;
import com.atguigu.gmall.model.product.SkuInfo;
import com.atguigu.gmall.product.client.ProductFeignClient;
import org.apache.lucene.search.join.ScoreMode;
import org.aspectj.weaver.ast.Var;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.nested.ParsedNested;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedLongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ListServiceImpl implements ListService {
    //ES的四种客户端

   /* //1. ES官方客户端(低级别)
    @Autowired
    private TransportClient transportClient;
    //2.spring官方客户端.底层封装了ES官方客户端(低级别)
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;*/
    //3.ES官方客户端(高)
    @Autowired
    private RestHighLevelClient restHighLevelClient; //查询
    //4.spring官方客户端.底层封装了ES官方客户端(高级别),现在都用高级别的
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate; //查询

    @Autowired
    private GoodsDao goodsDao; //添加 删除 修改

    @Autowired
    private ProductFeignClient  productFeignClient;

    @Autowired
    private RedisTemplate redisTemplate;


    //创建索引
    //创建mappings映射
    @Override
    public void createIndex() {
        //创建索引库
        elasticsearchRestTemplate.createIndex(Goods.class);//索引库名字固定,存在硬编码问题
        //mappings映射
        elasticsearchRestTemplate.putMapping(Goods.class);
        System.out.println(elasticsearchRestTemplate.toString());
    }

    //上架:将商品添加到索引库
    @Override
    public void onSale(Long skuId) {
        Goods goods = new Goods();
        //往goods中添加数据
        //1.添加库存信息
        SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
        goods.setId(skuInfo.getId());
        goods.setDefaultImg(skuInfo.getSkuDefaultImg());
        goods.setTitle(skuInfo.getSkuName());
        goods.setPrice(skuInfo.getPrice().doubleValue());
        //2.创建时间
        goods.setCreateTime(new Date());
        //3.一二三级分类
        BaseCategoryView baseCategoryView = productFeignClient.getBaseCategoryView(skuInfo.getCategory3Id());
        goods.setCategory1Id(baseCategoryView.getCategory1Id());
        goods.setCategory1Name(baseCategoryView.getCategory1Name());
        goods.setCategory2Id(baseCategoryView.getCategory2Id());
        goods.setCategory2Name(baseCategoryView.getCategory2Name());
        goods.setCategory3Id(baseCategoryView.getCategory3Id());
        goods.setCategory3Name(baseCategoryView.getCategory3Name());
        //4.品牌
        BaseTrademark baseTrademark =  productFeignClient.getBaseTrademark(skuInfo.getTmId());
        goods.setTmId(baseTrademark.getId());
        goods.setTmName(baseTrademark.getTmName());
        goods.setTmLogoUrl(baseTrademark.getLogoUrl());
        //5.库存和平台属性及值的关联信息,三表联查:sku_attr_value,base_attr_value,base_attr_info
        List<SkuAttrValue> skuAttrValueList = productFeignClient.getSkuAttrValueList(skuId);
        List<SearchAttr> attrsList = skuAttrValueList.stream().map(skuAttrValue -> {
            SearchAttr searchAttr = new SearchAttr();
            searchAttr.setAttrId(skuAttrValue.getBaseAttrInfo().getId());
            searchAttr.setAttrName(skuAttrValue.getBaseAttrInfo().getAttrName());
            searchAttr.setAttrValue(skuAttrValue.getBaseAttrValue().getValueName());
            return searchAttr;
        }).collect(Collectors.toList());
        goods.setAttrs(attrsList);
        //保存数据
        goodsDao.save(goods);


    }
    //下架:将商品从索引库中删除
    @Override
    public void cancelSale(Long skuId) {
        //删除
        goodsDao.deleteById(skuId);
    }

    //商品热度评分  更新操作 使用redis缓存,数据类型ZSet 有序集合
    @Override
    public void hotScore(Long skuId, Integer score) {
        String hotScore = "hotScore";
        //redis更新商品热度评分
        Double score1 = redisTemplate.opsForZSet().incrementScore(hotScore, skuId, score);
        if(score1 %10 == 0){
            Optional<Goods> byId = goodsDao.findById(skuId);
            Goods goods = byId.get();
            //追加分数
            goods.setHotScore(Math.round(score1));
            //3:更新索引库中的数据
            goodsDao.save(goods);
            System.out.println("当前商品:" + skuId + ":热度评分：" + score1 + ":更新索引库了");
        }
        System.out.println("当前商品:" + skuId + ":热度评分：" + score1);
    }
    //开始搜索
    @Override
    public SearchResponseVo search(SearchParam searchParam) {
        //1.搜索请求对象
        SearchRequest searchRequest = buildSearchRequest(searchParam);
        //2.开始搜索
        try {
            SearchResponse searchResponse =
                    restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            //3.解析搜索结果
            SearchResponseVo vo = parseSearchResponse(searchResponse);
            //3.1:计算总页数
            //总页数 == (总条数 + 每页数 -1)/每页数
            vo.setPageNo(searchParam.getPageNo());//当前页
            vo.setPageSize(searchParam.getPageSize());//每页记录数
            //总页数
            long totalPages = (vo.getTotal() + vo.getPageSize() - 1) / vo.getPageSize();
            vo.setTotalPages(totalPages);
            return vo;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //解析搜素结果
    private SearchResponseVo parseSearchResponse(SearchResponse searchResponse) {
        SearchResponseVo vo = new SearchResponseVo();
        //1.总条数
        SearchHits hits = searchResponse.getHits();
        long totalHits = hits.getTotalHits();
        System.out.println("总条数：" + totalHits);
        vo.setTotal(totalHits);
        //2.商品集合 偷天换日
        SearchHit[] hits1 = hits.getHits();
        List<Goods> goodsList = Arrays.stream(hits1).map(hit ->{
            ////商品信息
            String sourceAsString = hit.getSourceAsString();
            Goods goods = JSONObject.parseObject(sourceAsString, Goods.class);
            //高亮显示
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField highlightField = highlightFields.get("title");
            String highlightTitle = highlightField.getFragments()[0].toString();
            goods.setTitle(highlightTitle);
            return goods;
        }).collect(Collectors.toList());
        vo.setGoodsList(goodsList);
        //3.品牌集合 ParsedLongTerms :为强转过后的类型,好获取buckets
        ParsedLongTerms tmIdAgg = searchResponse.getAggregations().get("tmIdAgg");
        List<SearchResponseTmVo> trademarkList = tmIdAgg.getBuckets().stream().map(bucket -> {
            SearchResponseTmVo tmVo = new SearchResponseTmVo();
            //品牌id
            tmVo.setTmId( bucket.getKeyAsNumber().longValue());
            //品牌名称
            ParsedStringTerms tmNameAgg = ((Terms.Bucket) bucket).getAggregations().get("tmNameAgg");
            tmVo.setTmName(tmNameAgg.getBuckets().get(0).getKeyAsString());
            //品牌Logo
            ParsedStringTerms tmLogoUrlAgg = ((Terms.Bucket) bucket).getAggregations().get("tmLogoUrlAgg");
            tmVo.setTmLogoUrl(tmLogoUrlAgg.getBuckets().get(0).getKeyAsString());
            return tmVo;
        }).collect(Collectors.toList());
        vo.setTrademarkList(trademarkList);
        //4.平台属性集合
        //解析嵌套
        ParsedNested attrsAgg = searchResponse.getAggregations().get("attrsAgg");
        ParsedLongTerms attrIdAgg = attrsAgg.getAggregations().get("attrIdAgg");
        //平台属性集合
        List<SearchResponseAttrVo> attrsList = attrIdAgg.getBuckets().stream().map(bucket -> {
            SearchResponseAttrVo attrVo = new SearchResponseAttrVo();
            //平台属性id bucket:意思:桶
            attrVo.setAttrId(bucket.getKeyAsNumber().longValue());
            //平台属性名称
            ParsedStringTerms attrNameAgg = ((Terms.Bucket) bucket).getAggregations().get("attrNameAgg"); //获取子类聚合查询,并获取值
            attrVo.setAttrName(attrNameAgg.getBuckets().get(0).getKeyAsString()); //getKeyAsString():转化为字符串
            //平台属性值集合 :attrValueList
            ParsedStringTerms attrValueAgg = ((Terms.Bucket) bucket).getAggregations().get("attrValueAgg");
            //偷天换日attrValueAgg.getBuckets().stream().map:转换map的属性
            List<String> attrValueList = attrValueAgg.getBuckets().stream().map(Terms.Bucket::getKeyAsString
            ).collect(Collectors.toList());
            attrVo.setAttrValueList(attrValueList);
            return attrVo;
        }).collect(Collectors.toList());
        vo.setAttrsList(attrsList);
        return vo;
    }

    //开始搜索,搜索条件
    private SearchRequest buildSearchRequest(SearchParam searchParam) {
        /*
        * GET goods/_search
        * {
        *   "query":{
        *       "match_all":{}
        *            }
        * }
        * */
        //1.创建搜索请求对象
        SearchRequest searchRequest = new SearchRequest("goods");
        //2.构建搜索资源对象:query包裹的{}:大括号
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //组合查询条件,查询条件很多,查询条件must 相当于 and
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //组合的查询条件要放到大括号内
        searchSourceBuilder.query(boolQueryBuilder);
        //2.1查询关键词
        String keyword = searchParam.getKeyword();
        if(!StringUtils.isEmpty(keyword)){
            //根据关键字查询,match: 先分词-->在模糊查询
           boolQueryBuilder.must(QueryBuilders.matchQuery("title",keyword).operator(Operator.AND));
        }else {
            //全局搜索,没有关键词
            boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        }
        //2.2查询一二三级分类
        Long category1Id = searchParam.getCategory1Id();
        if(null != category1Id){
            //filter == must :过滤器
            boolQueryBuilder.filter(QueryBuilders.termQuery("category1Id",category1Id));
        }
        Long category2Id = searchParam.getCategory2Id();
        if(null != category2Id){
            //filter == must :过滤器
            boolQueryBuilder.filter(QueryBuilders.termQuery("category2Id",category2Id));
        }
        Long category3Id = searchParam.getCategory3Id();
        if(null != category3Id){
            //filter == must :过滤器
            boolQueryBuilder.filter(QueryBuilders.termQuery("category3Id",category3Id));
        }
        //2.3查询品牌
        String trademark = searchParam.getTrademark();
        if(!StringUtils.isEmpty(trademark)){
            String[] t = trademark.split(":"); // 0:id  1:名称
            boolQueryBuilder.filter(QueryBuilders.termQuery("tmId",t[0]));
        }
        //TODO 2.4 平台属性 超难
        //http://localhost:8090/path
        // ?keyword=手机&props=23:4G:运行内存&props=24:1200万:摄像头像素
        //Springmvc的课程了
        String[] props = searchParam.getProps();
        //props=23:4G:运行内存   prop:平台属性ID:平台属性值:平台属性名称
        if(props != null && props.length> 0){
            for (String prop : props) {
                //子组合对象
                BoolQueryBuilder subBoolQueryBuilder = QueryBuilders.boolQuery();
                String[] p = prop.split(":"); //分割
                if(p.length == 3){
                    //过滤查询
                    //平台属性ID
                    subBoolQueryBuilder.must(
                            QueryBuilders.termQuery("attrs.attrId",p[0]));
                    //平台属性值
                    subBoolQueryBuilder.must(
                            QueryBuilders.termQuery("attrs.attrValue",p[1]));
                }
                //父组合对象 添加 子组合对象  and  should:并集  must_not:非的关系
                boolQueryBuilder.filter(QueryBuilders.nestedQuery("attrs",subBoolQueryBuilder, ScoreMode.None));
            }
        }

        //组合的查询条件要放到大括号内
        searchSourceBuilder.query(boolQueryBuilder);

        //3.分页
        Integer pageNo = searchParam.getPageNo();//当前页
        Integer pageSize = searchParam.getPageSize();//每页记录数
        searchSourceBuilder.from((pageNo-1)*pageSize);//开始行
        searchSourceBuilder.size(pageSize);//每页记录数

        //4.排序 1:asc  1:desc  2:asc 2:desc ...
        String order = searchParam.getOrder();
        if(!StringUtils.isEmpty(order)){
            String[] o = order.split(":");//2  0: 1:
            String field = ""; //排序的域名
            switch (o[0]){  //取出的值是几,就对应什么字段
                case "1" : field = "hotScore";break;
                case "2" : field = "price";break;
                case "3" : field = "createTime";break;
            }
            //字段的第二个值判断是升序还是降序
            searchSourceBuilder.sort(field,"asc".equalsIgnoreCase(o[1]) ? SortOrder.ASC:SortOrder.DESC);
        }else {
            //默认是按照 综合  由高到低
            searchSourceBuilder.sort("hotScore", SortOrder.DESC);
        }

        //5.高亮显示
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("title") //关键字
                .preTags("<font style ='color:red'") //前缀
                .postTags("<font");//后缀
        searchSourceBuilder.highlighter(highlightBuilder);
        //6.品牌的集合 .ES的聚合查询:桶聚合,指标聚合.这里用桶聚合
        //查询 品牌id name logoUrl
        searchSourceBuilder.aggregation(AggregationBuilders.terms("tmIdAgg").field("tmId").size(100) //父类:确定没有重复,带着子类
                                        .subAggregation(AggregationBuilders.terms("tmNameAgg").field("tmName"))//子类
                                        .subAggregation(AggregationBuilders.terms("tmLogoUrlAgg").field("tmLogoUrl"))//子类
        );
        //平台属性聚合查询 使用桶聚合 内有嵌套关系
        searchSourceBuilder.aggregation(AggregationBuilders.nested("attrsAgg","attrs") //父类
                                        .subAggregation(AggregationBuilders.terms("attrIdAgg").field("attrs.attrId") //子类
                                        .subAggregation(AggregationBuilders.terms("attrNameAgg").field("attrs.attrName")) //孙子类
                                        .subAggregation(AggregationBuilders.terms("attrValueAgg").field("attrs.attrValue"))) //孙子类
        );


        //打印结果就是DSL语句
        System.out.println("DSL:" + searchSourceBuilder.toString());

        //6
        searchRequest.source(searchSourceBuilder);
        //指定索引库的名称   不指定查询所有索引库
        searchRequest.indices("goods");

        //返回搜索资源对象
        return searchRequest;

    }


}
