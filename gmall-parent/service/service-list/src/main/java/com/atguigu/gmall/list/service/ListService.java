package com.atguigu.gmall.list.service;


import com.atguigu.gmall.model.list.SearchParam;
import com.atguigu.gmall.model.list.SearchResponseVo;

public interface ListService {
    //创建索引库
    void createIndex();
    //上架:将商品添加到索引库
    void onSale(Long skuId);
    //下架:将商品从索引库中删除
    void cancelSale(Long skuId);
    //商品热度评分  更新操作
    void hotScore(Long skuId, Integer score);
    //开始搜索
    SearchResponseVo search(SearchParam searchParam);
}
