package com.atguigu.gmall.gateway.filter;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.gateway.constant.RedisConst;
import com.atguigu.gmall.result.Result;
import com.atguigu.gmall.result.ResultCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.MediaTray;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


//网关(全局过滤中)
/*
 * 1.判断用户是否登录
 * 2.去认证中心进行认账
 * 3.用户已登录 允许加入购物车
 * 4.用户未登录 要求用户去登录
 * */
public class LoginGlobalFilter implements GlobalFilter, Ordered {


    //路径匹配工具类
    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    private static final  String TOKEN = "token";
    //重定向的地址前部分
    public  static final String LOGINURL = "http://passport.gmall.com/login.html?originUrl=";

    @Autowired
    private RedisTemplate redisTemplate;
    //全局过滤器的执行方法

    @Value("${login.url}") //读取配置文件
    private  String[] loginUrl;


    //ServerWebExchange:服务网路交换器,提供对HTTP请求和响应的访问，
    // 并公开额外的 服务器 端处理相关属性和特性，如请求属性
    // 网关底层技术 ：  Spring官方文档上写的 WebFlux
    //不再是同学们学习的常用技术：Tomcat Servlet技术
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //使用交换器获得请求和响应
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //内部资源的路径
        //String s = request.getPath().toString(); 两种方法
        String path = request.getURI().getPath();
        System.out.println("路径:" + path);
        //同步请求的重定向路径
        String allPath = request.getPath().toString();
        System.out.println("完整的路径：" + allPath);

        //1.检验用户资源是否为内部资源
        //  /api/product/inner/getSkuInfo/{skuId}   这就是内部资源
        //参数1.:路径规则
        //参数2.内部资源 友情提示 无权访问  枚举

        if (antPathMatcher.match("/**/inner/**", path)) {
            //1.1不是内部资源,放行'
            //1.2内部资源,提示无权访问
            return out(response,ResultCodeEnum.PERMISSION);

        }
        //2:校验用户是否登录
        String userId = getUserId(request);
        //2.1: 是否登录：异步   未登录   Result对象 （信息 Code 数据）
        if(antPathMatcher.match("/**/auth/**",path)&&StringUtils.isEmpty(userId)){
        //需要登录的异步请求 同时 未登录
            return out(response,ResultCodeEnum.LOGIN_AUTH);
        }
        //2.2: 是否登录：同步   未登录  重定向到登录页面
        for (String url : loginUrl){ //循环配置文件内loginUrl的路径
            if(path.contains(url) && StringUtils.isEmpty(userId)){
                //需要登录的请求 同时 未登录 重定向到登录页面去
                response.setStatusCode(HttpStatus.SEE_OTHER);
                //重定向的路径
                try {
                    response.getHeaders().add(HttpHeaders.LOCATION,
                         LOGINURL+ URLEncoder.encode(allPath,"utf-8"));
                    //响应
                    return  response.setComplete();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        /*////3:准备放行的时候   如果不为空或NULL 可以传递用户的ID了  请求头
        if(!StringUtils.isEmpty(userId)){
                   //request.getHeaders().add("userId",userId);
                   request.mutate().header("userId",userId);
                }*/

        //放行
        return chain.filter(exchange);

    }

    //统一返回结果 抽取的
    private Mono<Void> out(ServerHttpResponse response,ResultCodeEnum resultCodeEnum) {
        Result<Object> result = Result.build(null, resultCodeEnum);
        String json = JSONObject.toJSONString(result);
        DataBufferFactory dataBufferFactory = response.bufferFactory();
        DataBuffer dataBuffer = dataBufferFactory.wrap(json.getBytes());
        //提示无权访问,解决乱码问题
        //getHeaders:请求头.类型和值
        //解决乱码问题  Content-Type: text/html; charset=utf-8  刷新页面
        //解决乱码问题  Content-Type: application/json; charset=utf-8  Ajax请求
        response.getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        return response.writeWith(Mono.just(dataBuffer));
    }

    //检验用户是否登录
    private String getUserId(ServerHttpRequest request) {
        //1.获取token,从请求头中获取
        String token = request.getHeaders().getFirst(TOKEN);
        if(StringUtils.isEmpty(token)){
            //请求头中没获取到.,从缓存中获取,先获取缓存
            HttpCookie httpCookie
                    = request.getCookies().getFirst(TOKEN);
            if(httpCookie != null){
                //从缓存中获取token
                token= httpCookie.getValue();
            }
        }
        if(!StringUtils.isEmpty(token)){
            //1.2获取到令牌
            //2.通过令牌获取缓存中的UserId
            if(redisTemplate.hasKey(RedisConst.USER_LOGIN_KEY_PREFIX+token)){
                //2.1有  直接返回
                return (String)   redisTemplate.opsForValue().get(RedisConst.USER_LOGIN_KEY_PREFIX+token);
            }
        }
        //2.1 没有返回null
        return null;
    }

    /**
     * LoginGlobalFilter : 我们自定义的全局过滤器
     * <p>
     * GateWay网关 ： 默认自带 9大过滤器
     * 完成网关的基础功能
     * 断言
     * 路由
     * ....
     * 共用 10大过滤器 == 9个自带 + 1个自己写的
     * <p>
     * 大约 3~4个是优先执行   自定义的建议在大约中间执行      大约是5后执行
     * 负整数最大      --- -1   --- 0     ---      1 --------- 正整数最大
     * 优化级最大  ---------------------------------------  优化级最小
     *
     * @return
     */
    //此全局过滤器的排序
    @Override
    public int getOrder() {
        return 0;
    }
}
