package com.atguigu.gmall.model.list;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *  映射对象   映射到ES索引库    Lucene实现在  Document文档类型  数据类型
 */
@Document(indexName = "goods", type = "info", shards = 1, replicas = 0)
@Data
public class Goods implements Serializable {

    @Id
    private Long id;

    //下面三个来自库存表
    @Field(type = FieldType.Keyword, index = false)
    private String defaultImg;//type=FieldType 二种 分

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String title;

    @Field(type = FieldType.Double)
    private Double price;

    @Field(type = FieldType.Date)
    private Date createTime; // 新品

    //下面三个来自base_trademark表
    @Field(type = FieldType.Long)
    private Long tmId;

    @Field(type = FieldType.Keyword)
    private String tmName;

    @Field(type = FieldType.Keyword)
    private String tmLogoUrl;
    //下面6个来自分类视图表
    @Field(type = FieldType.Long)
    private Long category1Id;

    @Field(type = FieldType.Keyword)
    private String category1Name;

    @Field(type = FieldType.Long)
    private Long category2Id;

    @Field(type = FieldType.Keyword)
    private String category2Name;

    @Field(type = FieldType.Long)
    private Long category3Id;

    @Field(type = FieldType.Keyword)
    private String category3Name;

    //用户评价
    @Field(type = FieldType.Long)
    private Long hotScore = 0L;  //综合 排序 热点评分 公平原则 联系

    //来自sku_attr_value
    @Field(type = FieldType.Nested)
    private List<SearchAttr> attrs;//平台属性集合

}
