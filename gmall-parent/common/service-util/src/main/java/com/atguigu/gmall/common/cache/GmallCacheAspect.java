package com.atguigu.gmall.common.cache;

import com.atguigu.gmall.common.constant.RedisConst;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

//自定义缓存注解@GmallCache;  Aspect:切面
@Aspect //切面注解
@Component //实例化注解
public class GmallCacheAspect {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RedissonClient redissonClient;

    //面向切面编程:业务逻辑各部分之间的耦合度降低,提高程序的可复用性
    //环绕式     任意返回值        任意包 任意类 任意方法 任意形参
    //@Around("execution(public * *.*.*(..))")
    //由此注解的方法都需要进入到下面的切面方法中执行,  方法前 方法后 返回值后 抛出异常后
    @Around("@annotation(com.atguigu.gmall.common.cache.GmallCache)")
    public Object cacheAround(ProceedingJoinPoint pjp){
        //准备工作,获取cacheKey=前缀+变量+后缀
        //1.获取前缀    signature:签名==包装+类名+方法名+形参
        MethodSignature methodSignature = (MethodSignature)pjp.getSignature();
        Method method = methodSignature.getMethod();
        //获取方法上注解
        GmallCache gmallCache = method.getAnnotation(GmallCache.class);
        //获取注解的前缀
        String prefix = gmallCache.prefix();
        //2.获取形参.方法中形参列表中形参个数不同
        Object[] args = pjp.getArgs();
        //缓存key;   Arrays.asList(args):所以用数组
        String cacheKey = prefix +":" + Arrays.asList(args)+ RedisConst.SKUKEY_SUFFIX;
        //1.查询缓存
        Object result = redisTemplate.opsForValue().get(cacheKey);
        //2.缓存中由直接返回
        //3.缓存中没有,则查询DB
        if( null == result){
            //3.1.加分布式锁
            String cacheLockKey = prefix + ":" +Arrays.asList(args) +RedisConst.SKULOCK_SUFFIX;
            RLock lock = redissonClient.getLock(cacheLockKey);
            try {
                //3.2获取锁 ,可重入锁
                boolean isLock = lock.tryLock(RedisConst.SKULOCK_EXPIRE_PX1, RedisConst.SKULOCK_EXPIRE_PX2, TimeUnit.SECONDS);
                if(isLock){
                    try {
                        result = pjp.proceed();//从切面中返回方法查询DB
                        if(null != result){
                            //4.获取到锁查询DB
                            //4.1.查询DB,并把一份存储到缓存中,缓存雪崩问题,加过期时间并加随机数
                            redisTemplate.opsForValue().set(cacheKey,result,RedisConst.SECKILL__TIMEOUT
                                    +new Random().nextInt(300),TimeUnit.SECONDS);
                        }else {
                            //5.DB中没有数据,创建新的数据,并储存到缓存中,缓存穿透
                            result = method.getReturnType().newInstance();
                            redisTemplate.opsForValue().set(cacheKey,result,5,TimeUnit.MINUTES);
                        }
                    } finally {
                        //6.解锁
                        lock.unlock();
                    }
                }else{
                    //7,未获取到锁,睡一会,查询缓存
                    TimeUnit.SECONDS.sleep(1);
                    result = redisTemplate.opsForValue().get(cacheKey);
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        //8.返回结果
        return result;

    }
}
