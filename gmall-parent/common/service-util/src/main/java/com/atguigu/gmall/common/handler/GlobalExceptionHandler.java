package com.atguigu.gmall.common.handler;


import com.atguigu.gmall.exception.GmallException;
import com.atguigu.gmall.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.BindException;

/**
 * 全局异常处理类
 * 整个项目只要抛出异常 全局异常处理器注解:@ControllerAdvice
 *  controller 向上抛
 *  service 向上抛
 *  mapper 向上抛
 *  已经自定义异常处理 try{} catch 就不用处理
 *
 *  //异常抛出优先级:抛出的异常与指定的异常更相符就优先处理
 *
 */
@ControllerAdvice //全局异常处理注解 SSM阶段
@Slf4j
public class GlobalExceptionHandler {

    //处理绑定异常 参数不能为空
    @ExceptionHandler(BindException.class)
    @ResponseBody
    public  Result bindError(BindException e){

        log.error("参数异常");//日志打印异常
        return  Result.fail();//将异常返回到页面上  抛异常到页面
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody //此注解 直接把返回结果抛到页面上    //去掉此注解 ：可以跳转指定页面
    public Result error(Exception e){
        e.printStackTrace();//打印在控制台
        return Result.fail(); //将异常返回到页面上  抛异常到页面
        //企业做法： 跳转错误页面   服务器太忙  美工前端 美丽页面 提示
    }

    /**
     * 自定义异常处理方法
     * @param e
     * @return
     */
    @ExceptionHandler(GmallException.class)
    @ResponseBody
    public Result error(GmallException e){
        return Result.fail(e.getMessage());
    }
}
