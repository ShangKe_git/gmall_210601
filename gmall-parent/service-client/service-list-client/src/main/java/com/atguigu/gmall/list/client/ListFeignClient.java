package com.atguigu.gmall.list.client;

import com.atguigu.gmall.model.list.SearchParam;
import com.atguigu.gmall.model.list.SearchResponseVo;
import com.atguigu.gmall.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "service-list")
public interface ListFeignClient {

    //商品热度评分  更新操作
    @GetMapping("/api/list/hotScore/{skuId}/{score}")
    public Result hotScore(@PathVariable Long skuId, @PathVariable Integer score);

    //开始搜索
    @PostMapping("/api/list/search")
    public SearchResponseVo search(@RequestBody SearchParam searchParam);
}

