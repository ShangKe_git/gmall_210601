package com.atguigu.gmall.product.client;

import com.atguigu.gmall.model.product.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author 李旭
 * @date 2021/11/19 10:09
 * @Description:
 */
@FeignClient(name = "service-product")
public interface ProductFeignClient {


    //1:库存信息 及库存的所有图片  条件：库存Id
    // inner:表示自己这个微服务提供数据是内部数据  不可以暴露到外网上
    @GetMapping("/api/product/inner/getSkuInfo/{skuId}")
    public SkuInfo getSkuInfo(@PathVariable Long skuId);
    //2、上面的库存信息中有三级分类的ID  所以才可以查询一二三级分类
    @GetMapping("/api/product/inner/getBaseCategoryView/{category3Id}")
    public BaseCategoryView getBaseCategoryView(@PathVariable Long category3Id);
    //3:要求价格单独查询   从MySQL中查询
    @GetMapping("/api/product/inner/getPrice/{skuId}")
    public BigDecimal getPrice(@PathVariable Long skuId);
    //4:查询销售属性及属性值并选中
    @GetMapping("/api/product/inner/selectSpuSaleAttrListCheckBySku/{spuId}/{skuId}")
    public List<SpuSaleAttr> selectSpuSaleAttrListCheckBySku(@PathVariable Long spuId
            , @PathVariable Long skuId);
    //5:组合数据 对应 skuId
    @GetMapping("/api/product/inner/getSaleAttrValuesBySpu/{spuId}")
    public Map getSaleAttrValuesBySpu(@PathVariable Long spuId);
    //6:查询分类视图中所有的分类数据
    //6:查询分类视图中所有的分类数据
    @GetMapping("/api/product/inner/getBaseCategoryViewList")
    public List<BaseCategoryView> getBaseCategoryViewList();

    //7:根据品牌ID 查询品牌集合
    @GetMapping("/api/product/getBaseTrademark/{tmId}")
    public BaseTrademark getBaseTrademark(@PathVariable Long tmId);

    //8:skuInfo与平台属性及值的关联数据
    @GetMapping("/api/product/inner/getSkuAttrValueList/{skuId}")
    public List<SkuAttrValue> getSkuAttrValueList(@PathVariable Long skuId);
}
