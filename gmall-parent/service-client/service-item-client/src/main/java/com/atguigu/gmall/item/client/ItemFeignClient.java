package com.atguigu.gmall.item.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

//商品详情微服务 暴露接口  被 页面微服务web-all 调用
@FeignClient(name = "service-item")
public interface ItemFeignClient {

    //数据汇总  处理
    @GetMapping("/api/item/getItem/{skuId}")
    public Map getItem(@PathVariable Long skuId);
}
